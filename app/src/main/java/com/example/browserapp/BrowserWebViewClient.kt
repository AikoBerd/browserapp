package com.example.browserapp

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

class BrowserWebViewClient(private val activity: AppCompatActivity) : WebViewClient() {
    //in the app load
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        view.loadUrl(request.url.toString())
        return true
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        val title = view?.title ?: activity.title
        activity.supportActionBar?.title = title
    }
}
