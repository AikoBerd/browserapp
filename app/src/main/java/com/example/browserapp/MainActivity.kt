package com.example.browserapp

import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    private lateinit var searchBtn:Button
    private var defaultColor: Int = R.color.blue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val webView: WebView = findViewById(R.id.webView)
        val urlEditText: EditText = findViewById(R.id.urlEditText)

        searchBtn = findViewById(R.id.searchBtn)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            searchBtn.setBackgroundColor(getColor(R.color.blue))
        }

        searchBtn.setOnClickListener {
            val url = urlEditText.text.toString()
            webView.webViewClient = BrowserWebViewClient(this)
            webView.loadUrl(url)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(BTN_COLOR_KEY, defaultColor)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        defaultColor = savedInstanceState.getInt(BTN_COLOR_KEY)
        searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
         when (item.itemId) {
            R.id.blue -> {
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
                defaultColor = R.color.blue
            }
            R.id.green -> {
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
                defaultColor = R.color.green
            }
            R.id.violet -> {
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.violet))
                defaultColor = R.color.violet
            }

            R.id.brown -> {
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.brown))
                defaultColor = R.color.brown
            }
            R.id.yellow -> {
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, R.color.yellow))
                defaultColor = R.color.yellow
            }
            R.id.orange -> {
                defaultColor = R.color.orange
                searchBtn.setBackgroundColor(ContextCompat.getColor(this, defaultColor))

            }
            else -> super.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    companion object{
        private const val BTN_COLOR_KEY = "Btn Color Key"
    }
}